/* 
 * File:   sd.h
 * Author: urakaminaoya
 *
 * Created on September 5, 2017, 10:45 PM
 */

#ifndef SD_H
#define	SD_H

#include "spi_pins.h"
#include "spi_mode_3.h"

#define RETRY_COUNT 250
#define GO_IDLE_STATE 0x40
#define SEND_OP_COND 0x41
#define WRITE_BLOCK 0x58
#define READ_SINGLE_BLOCK 0x51

typedef struct sd_communication_information{
    uint32_t block_number;  //should be 3byte
    uint8_t data_size; //should be 1byte
    uint16_t start_byte_number; //Maybe, we don't need.
    volatile unsigned char *port;
    unsigned char pin;
} sd_communication_information_t;

typedef union block_number_detach {
    uint32_t blocks;
    struct {
        uint8_t first_byte;
        uint8_t second_byte;
        uint8_t third_byte;
        uint8_t forth_byte;
    };
} block_number_detach_t;

typedef enum set_mode {
    WRITE,
    READ
} set_mode_t;

block_number_detach_t target;

static void sd_idling(volatile unsigned char *port, unsigned char pin);
static int sdcard_response(uint8_t response);
static uint8_t sd_setup_SPImode(sd_communication_information_t* sd_condition);
static void sd_communicate(sd_communication_information_t* sd_condition ,uint8_t* data, set_mode_t mode);
static void send_blockNum_command(sd_communication_information_t* sd_condition , set_mode_t mode);
static int Write_SDcard_part(sd_communication_information_t* sd_condition, uint8_t* tx_data);
static int Read_SDcard_part(sd_communication_information_t* sd_condition, uint8_t* rx_data);


int Write_SDcard(sd_communication_information_t* sd_condition, uint8_t* tx_data);
int Read_SDcard(sd_communication_information_t* sd_condition, uint8_t* rx_data);

#endif	/* SD_H */

