
// PIC16F877 Configuration Bit Settings

// 'C' source line config statements

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bits (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF         // Low Voltage In-Circuit Serial Programming Enable bit (RB3/PGM pin has PGM function; low-voltage programming enabled)
#pragma config CPD = OFF        // Data EE Memory Code Protection (Code Protection off)
#pragma config WRT = OFF        // FLASH Program Memory Write Enable (Unprotected program memory may not be written to by EECON control)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.
#define _XTAL_FREQ     20000000

#include <xc.h>
#include "sd.h"
#include "TenkouGPIO.h"
#include "uart.h"
#include "spi_pins.h"
#include "spi_mode_3.h"

int main(void) {
    uint8_t rx_data;
    uint8_t tx_data;
    
    sd_communication_information_t sd_condition;
        sd_condition.port = &PORTD;
        sd_condition.pin = 7;
        sd_condition.data_size = 4;
        sd_condition.start_byte_number = 0;
    for(;;) {
        sd_condition.block_number = 56;
        Write_SDcard(&sd_condition,&rx_data);
        Read_SDcard(&sd_condition,&tx_data);
        
    }

    
}
