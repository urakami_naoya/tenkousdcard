/*
 * File:   sd.c
 * Author: Urakami
 *
 * Created on November 28, 2017,  10:00 am
 */
#include "sd.h"

static void sd_idling(volatile unsigned char *port, unsigned char pin) {
    uint8_t dumy = 0xFF;
    setPinHigh(port, pin);
    
    SPI_CK_MASTER = 1;
    for (uint8_t loop = 0 ; loop < 75 ; loop++) {
        spiWrite_mode3(&dumy,1);
    }
    SPI_CK_MASTER = 0;
}

static int sdcard_response(uint8_t response) {   
    uint8_t command_response;
    SPI_MOSI_MASTER = 1;
    _delay(100);
    for(uint8_t count = 0 ; count < RETRY_COUNT ; count++) {           
        spiRead_mode3(&command_response,1); //Receive command response
        if(command_response == response){
            return -1;            
        }
    }
    return 0;
}

static uint8_t sd_setup_SPImode(sd_communication_information_t* sd_condition) {
    sd_idling(sd_condition->port,sd_condition->pin);
    
    uint8_t command , response , success_count = 0;    
    for (uint8_t time = 1 ; time <= 2 ; time++) {            
        switch(time) {
            case 1: 
                command = GO_IDLE_STATE;
                response = 0x01;
                break;
            case 2:  
                command = SEND_OP_COND;
                response = 0x00;
                break;
        }
       
        select_slave(sd_condition->port, sd_condition->pin, 0); 
        for(uint8_t retry_times = 0; retry_times < RETRY_COUNT; retry_times++) {
            spiWrite_mode3(&command,1);    
            uint8_t fill_part = 0x00;
            for(uint8_t i = 0 ; i < 4 ; i++) {
                spiWrite_mode3(&fill_part,1);
            }
            fill_part = 0x95;
            spiWrite_mode3(&fill_part,1);  
            if(sdcard_response(response)) {  //Check whichever command response is correct or not.
                success_count++;
                break;
            }
        } 
        SPI_CK_MASTER = 0;
        setPinHigh(sd_condition->port, sd_condition->pin);
    }
    return success_count;
}

static void sd_communicate(sd_communication_information_t* sd_condition ,uint8_t* data, set_mode_t mode) {
    uint16_t start_byte;
    switch(mode) {
        case WRITE: 
            start_byte = 0;
            spiWrite_mode3(data,sd_condition->data_size);
            SPI_MOSI_MASTER = 1; 
        break;
        
        case READ:
            start_byte = sd_condition->start_byte_number;
            if (start_byte != 0 ){
                spiRead_mode3(data,start_byte - 1);   //Move starting point to read data 
            }
            spiRead_mode3(data,sd_condition->data_size);
        break;
    };
    
    for(uint16_t bytes = 0 ; bytes < 512 - sd_condition->data_size - start_byte ; bytes++) {
        for(uint8_t clock = 0 ; clock < 8 ; clock++) {
            SPI_CK_MASTER = 0; 
            (mode == 0) ? _delay(15) : _delay(20);
            SPI_CK_MASTER = 1;
            (mode == 0) ? _delay(20) : _delay(10);
        }
    }
}

static void send_blockNum_command(sd_communication_information_t* sd_condition , set_mode_t mode) {
        uint8_t command , command_part;
        target.blocks = sd_condition->block_number * 512;
        switch(mode) {                
            case WRITE:
                command = WRITE_BLOCK;
                break;
                
            case READ:
                command = READ_SINGLE_BLOCK;
                break;
        }
            spiWrite_mode3(&command,1); //COMMAND

            command_part = target.forth_byte;
            spiWrite_mode3(&command_part,1);
            
            command_part = target.third_byte;
            spiWrite_mode3(&command_part,1);
            
            command_part = target.second_byte;
            spiWrite_mode3(&command_part,1);
            
            command_part = target.first_byte;
            spiWrite_mode3(&command_part,1);
            
            command_part = 0xFF;    //CRC
            spiWrite_mode3(&command_part,1);
}

static int Write_SDcard_part(sd_communication_information_t* sd_condition, uint8_t* tx_data) {
    select_slave(sd_condition->port, sd_condition->pin, 0);
    for(uint8_t count = 0 ; count < RETRY_COUNT ; count++) {
        SPI_CK_MASTER = 1;    
        
        send_blockNum_command(sd_condition,WRITE);

        if(sdcard_response(0x00)) {
            for(uint8_t loop = 0 ; loop < RETRY_COUNT ; loop++) { 
                uint8_t sign = 0xFE;    //Send start_byte(0xFE)
                spiWrite_mode3(&sign,1);    
                
                sd_communicate(sd_condition,tx_data,WRITE);  //Write data to SD card.
                
                uint8_t polling = 0x00;  //Send CRC(2bytes)
                spiWrite_mode3(&polling,1); 
                spiWrite_mode3(&polling,1);
                
                uint8_t command_response;
                spiRead_mode3(&command_response,1);   
                
                if((command_response & 0x1F) == 0x05) {
                    for(uint8_t i = 0; i < 16; i++) {// 15 cycles was measured by osciloscop and while loop. The value should be more than 15
                        spiRead_mode3(&command_response,1); //Read data_response 
                    }                    
                    SPI_CK_MASTER = 0; 
                    setPinHigh(sd_condition->port, sd_condition->pin);                    
                    return 0;
                }
            } 
        }
        SPI_CK_MASTER = 0; 
    }
    setPinHigh(sd_condition->port, sd_condition->pin);
    return -1;
}

static int Read_SDcard_part(sd_communication_information_t* sd_condition, uint8_t* rx_data) {
    select_slave(sd_condition->port, sd_condition->pin, 0);
    for(uint8_t count = 0 ; count < RETRY_COUNT ; count++) {
        SPI_CK_MASTER = 1; 
        
        send_blockNum_command(sd_condition,READ);
        
        if(sdcard_response(0x00)){
            __delay_ms(1);
            if(sdcard_response(0xFE)) {
                sd_communicate(sd_condition,rx_data, READ);    //Read data from SD 
                uint8_t crc;
                spiRead_mode3(&crc,2);   //Read CRC data
                SPI_CK_MASTER = 0; 
                setPinHigh(sd_condition->port, sd_condition->pin);
                return 0;
            }
        }
        SPI_CK_MASTER = 0;
    }
    setPinHigh(sd_condition->port, sd_condition->pin);
    return -1;
}

int Write_SDcard(sd_communication_information_t* sd_condition, uint8_t* tx_data) {
    if(sd_setup_SPImode(sd_condition) != 2) {
        return 0;  //error should 
    };
    
    if(Write_SDcard_part(sd_condition, tx_data)) {
        return 0;  //error 
    };
    return -1;
}

int Read_SDcard(sd_communication_information_t* sd_condition, uint8_t* rx_data) {
    if(sd_setup_SPImode(sd_condition) != 2) {
        return 0;  //error
    };
    
    if(Read_SDcard_part(sd_condition,rx_data)) {
        return 0;  //error
    };
    
    return -1;
}